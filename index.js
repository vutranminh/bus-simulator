var simulator = require('./simulator');
var fs = require('fs');

function readLines(input, func) {
    var remaining = '';

    input.on('data', function (data) {
        remaining += data;
        var index = remaining.indexOf('\n');
        while (index > -1) {
            var line = remaining.substring(0, index);
            remaining = remaining.substring(index + 1);
            func(line);
            index = remaining.indexOf('\n');
        }
    });

    input.on('end', function () {
        if (remaining.length > 0) {
            func(remaining);
        }
    });
}

function start() {

    if (process.argv.length === 3) {
        console.log('--- Simulator reads input from file ---');
        var filename = process.argv[2];
        var input = fs.createReadStream(filename);
        readLines(input, function (commandStr) {
            if (commandStr.trim().length > 0) {
                try {
                    simulator.readCommand(commandStr);
                } catch (err) {
                    console.log(err);
                }
            }
        });
    } else if (process.argv.length == 2) {
        console.log('--- Simulator reads input from standard input ---');
        console.log('Enter one of following commands or Ctrl+C to quit:');
        console.log('PLACE X,Y,F');
        console.log('MOVE');
        console.log('LEFT');
        console.log('RIGHT');
        console.log('REPORT');

        process.stdin.resume();
        process.stdin.setEncoding('utf8');
        var util = require('util');

        process.stdin.on('data', function (text) {
            try {
                simulator.readCommand(text);
            } catch (err) {
                console.log(err);
            }
        });
    } else {
        console.log('--- Run simulator ---');
        console.log('node index.js --> simulator reads from standard input');
        console.log('node index.js [filename] --> simulator reads input from a file');
    }
}

start();


