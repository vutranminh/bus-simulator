const DIMENSION = 5;
const NORTH = {x: 0, y: 1};
const EAST = {x: 1, y: 0};
const SOUTH = {x: 0, y: -1};
const WEST = {x: -1, y: 0};
const NODIR = {x: 0, y: 0};
const DIRECTIONS = {NORTH: NORTH, EAST: EAST, SOUTH: SOUTH, WEST: WEST, NODIR: NODIR};

var simulator = {x: undefined, y: undefined, dir: NODIR};

function isPosValid(pos) {
    return 0 <= pos && pos < DIMENSION;
}
function sameDir(dir1, dir2) {
    return dir1 && dir2 && dir1.x == dir2.x && dir1.y == dir2.y;
}
function isDirValid(newDir) {
    return sameDir(newDir, NORTH) || sameDir(newDir, EAST) || sameDir(newDir, SOUTH) || sameDir(newDir, WEST);
}
function isPlaceValid(newX, newY, newDir) {
    return isPosValid(newX) && isPosValid(newY) && isDirValid(newDir);
}

simulator.DIRECTIONS = DIRECTIONS;

simulator.place = function (newX, newY, newDirStr) {
    var newDir = DIRECTIONS[newDirStr];
    this.unsetBus();
    if (isPosValid(newX, newY, newDir)) {
        this.x = newX;
        this.y = newY;
        this.dir = newDir;
    }
}

simulator.move = function () {
    if (!isPlaceValid(this.x, this.y, this.dir)) {
        return;
    }
    var newX = this.x + this.dir.x;
    var newY = this.y + this.dir.y;
    if (isPosValid(newX) && isPosValid(newY)) {
        this.x = newX;
        this.y = newY;
    }
}
simulator.rotate = function (lr) { //lr = left or right
    if (!isPlaceValid(this.x, this.y, this.dir)) {
        return;
    }
    //(x, y) rotated 90 degrees around (0, 0) is (-y, x) . If you want to rotate clockwise, you simply do it the other way around, getting (y, -x)
    lr = lr.toUpperCase();
    if (lr == 'LEFT') { //anti-clockwise
        this.dir = {x: this.dir.y == 0 ? 0 : -this.dir.y, y: this.dir.x};
    } else if (lr == 'RIGHT') { //clockwise
        this.dir = {x: this.dir.y, y: this.dir.x == 0 ? 0 : -this.dir.x};
    } else {
        throw 'INVALID ROTATE';
    }
}
simulator.report = function () {
    if (!isPlaceValid(this.x, this.y, this.dir)) {
        return;
    }
    var dirStr = 'UNKNOWN';
    if (sameDir(this.dir, NORTH)) {
        dirStr = 'NORTH';
    } else if (sameDir(this.dir, EAST)) {
        dirStr = 'EAST';
    } else if (sameDir(this.dir, SOUTH)) {
        dirStr = 'SOUTH';
    } else if (sameDir(this.dir, WEST)) {
        dirStr = 'WEST';
    }
    var output = 'Output:' + this.x + ',' + this.y + ',' + dirStr;
    console.log(output);
    return output;
}
simulator.readCommand = function (commandStr) {
    var $this = this;
    commandStr = commandStr.toUpperCase().trim();
    if (commandStr.indexOf('PLACE') == 0) {
        var args = commandStr.substr('PLACE'.length).trim().split(',');
        $this.place(parseInt(args[0]), parseInt(args[1]), args[2]);
    } else if (commandStr == 'MOVE') {
        $this.move();
    } else if (commandStr == 'LEFT' || commandStr == 'RIGHT') {
        $this.rotate(commandStr);
    } else if (commandStr == 'REPORT') {
        $this.report();
    } else {
        throw 'INVALID command';
    }
}
simulator.unsetBus = function () {
    this.x = undefined;
    this.y = undefined;
    this.dir = NODIR;
}
module.exports = simulator;
