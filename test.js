var simulator = require('./simulator');
var assert = require('assert');
var chai = require('chai');
describe('Test Simulator', function () {
    describe('Test PLACE', function () {
        beforeEach(function (done) {
            simulator.unsetBus();
            done();
        });
        it('PLACE should correctly put bus into the carpark with correct parameters', function () {
            simulator.place(1, 2, 'NORTH');
            assert.equal(simulator.x, 1);
            assert.equal(simulator.y, 2);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.NORTH);

            simulator.place(3, 3, 'WEST');
            assert.equal(simulator.x, 3);
            assert.equal(simulator.y, 3);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.WEST);

        });

        it('discard all commands in the sequence until a valid PLACE command has been executed', function () {
            simulator.place(-1, 3, 'NORTH');
            simulator.move();
            simulator.rotate('LEFT');
            simulator.move();
            simulator.rotate('RIGHT');
            chai.expect(simulator.x).to.be.undefined;
            chai.expect(simulator.y).to.be.undefined;
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.NODIR);

            simulator.place(2, 2, 'SOUTH');
            simulator.move();
            simulator.rotate('LEFT');
            simulator.move();
            assert.equal(simulator.x, 3);
            assert.equal(simulator.y, 1);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.EAST);
        });

    });
    describe('Test MOVE', function () {
        beforeEach(function (done) {
            simulator.unsetBus();
            done();
        });
        it('will move the bus one unit forward in the direction it is currently facing', function () {
            simulator.place(0, 0, 'NORTH');
            assert.equal(simulator.x, 0);
            assert.equal(simulator.y, 0);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.NORTH);
            simulator.move();
            assert.equal(simulator.x, 0);
            assert.equal(simulator.y, 1);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.NORTH);

            simulator.place(0, 0, 'EAST');
            assert.equal(simulator.x, 0);
            assert.equal(simulator.y, 0);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.EAST);
            simulator.move();
            simulator.move();
            assert.equal(simulator.x, 2);
            assert.equal(simulator.y, 0);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.EAST);
        });

        it('Any move that would cause the bus to exit the carpark must be ignored', function () {
            simulator.place(4, 4, 'NORTH');
            assert.equal(simulator.x, 4);
            assert.equal(simulator.y, 4);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.NORTH);
            simulator.move();
            assert.equal(simulator.x, 4);
            assert.equal(simulator.y, 4);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.NORTH);

            simulator.place(0, 0, 'WEST');
            assert.equal(simulator.x, 0);
            assert.equal(simulator.y, 0);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.WEST);
            simulator.move();
            assert.equal(simulator.x, 0);
            assert.equal(simulator.y, 0);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.WEST);

        });

    });

    describe('Test ROTATE', function () {
        beforeEach(function (done) {
            simulator.unsetBus();
            done();
        });
        it('LEFT and RIGHT will rotate the bus 90 degrees in the specified direction without changing the position of the bus', function () {
            simulator.place(0, 0, 'NORTH');
            assert.equal(simulator.x, 0);
            assert.equal(simulator.y, 0);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.NORTH);
            simulator.rotate('LEFT');
            assert.equal(simulator.x, 0);
            assert.equal(simulator.y, 0);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.WEST);

            simulator.rotate('RIGHT');
            assert.equal(simulator.x, 0);
            assert.equal(simulator.y, 0);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.NORTH);

            simulator.rotate('RIGHT');
            assert.equal(simulator.x, 0);
            assert.equal(simulator.y, 0);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.EAST);

            simulator.move();
            simulator.move();
            simulator.rotate('RIGHT');
            assert.equal(simulator.x, 2);
            assert.equal(simulator.y, 0);
            chai.expect(simulator.dir).to.deep.equal(simulator.DIRECTIONS.SOUTH);

        });

    });

    describe('Test REPORT', function () {
        beforeEach(function (done) {
            simulator.unsetBus();
            done();
        });
        it('will announce the X,Y and F of the bus', function () {
            simulator.place(1, 1, 'NORTH');
            simulator.rotate('LEFT');
            simulator.move();
            simulator.move();
            simulator.rotate('LEFT');
            assert.equal(simulator.report(), 'Output:' + simulator.x + ',' + simulator.y + ',' + 'SOUTH');

            simulator.move();
            simulator.rotate('RIGHT');
            simulator.move();
            assert.equal(simulator.report(), 'Output:' + simulator.x + ',' + simulator.y + ',' + 'WEST');
        });

        it('invalid PLACE will discard sequential REPORT', function () {
            simulator.place(-1, 1, 'NORTH');
            simulator.rotate('LEFT');
            simulator.move();
            simulator.move();
            simulator.rotate('LEFT');
            chai.expect(simulator.report()).to.be.undefined;
        });
    });
});