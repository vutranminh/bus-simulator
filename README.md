# bus-simulator
The application is a simulation of a robot operated bus moving in a carpark, of dimensions 5 units x 5 units

# how to start

* NodeJS 6.x.x required to run 

* check out the repository 

```
git clone https://vutranminh@bitbucket.org/vutranminh/bus-simulator.git
```

* install dependencies
```
npm install
```

* run the console program
```
node index.js -> simulator reads input from standard input
node index.js [filename] --> simulator reads input from a file
```

* run unit tests, required globally installed mocha
```
npm install -g mocha
```

then 
```
mocha test
```





